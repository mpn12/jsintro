/*DataType exercise
In this exercise, we are going to create our own superhero character!
Remember to use console.log to help you debug*/

//First, declare an empty OBJECT variable called 'superhero'


/*Give the 'superhero' a first name, last name, superhero name, HP(hit points), 
3 stengthes in an ARRAY, 3 weaknesses in an ARRAY, a motto */


//Set the superhero's alive status to true



/*Now console.log to introduce the character using the 'superhero' OBJECT
For example, 'Hi, I'm Quick Hands, I'm your handy man! 
My strenthes are Rock, Paper and Scissors. My weaknesses are Paper, Scissors and Rock.' */



/*One day, the superhero is on the way to rescue a kitten on the tree, a monster shows up to fight.
Create 4 BOOLEAN variables: hero, monster, hit, miss, and assign them the correct value so that:
superhero and monster cannot both win; either superhero or monster wins;
superhero always hits and never miss; 
monster always misses and never hit.
At the end, console.log superhero's victory.*/



/*The superhero trips over a cardboard box, and loses half of the HP,
loses all the strengthes and gains a weakness: cardboard box.
console.log the new superhero status and make sure the hero is still alive.*/


/*BONUS: create a monster character. 
Give both the monster and the hero attack and defense value. 
Now let them battle till the end. The hero might die this time ;(
*/