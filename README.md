# JSIntro

Welcome to Javascript Introduction Workshop! In this repository, you will find two modules: JSIntro1 and JSIntro2. 

JSIntro1 introduces JS data types like numbers, strings, booleans, arrays and objects; logic like if-else statements, loops; functions like how to declare and use a function. 

JSIntro2 shows you how to use JS to allow user's interaction in front-end development along with HTML and CSS. It introduces DOM(Document Object Model) and libraries like jQuery to help you manipulate HTML elements and change CSS properties. As a bonus, it briefly explains how to use AJAX to send HTTP requests. 

Hope you enjoy this workshop! 

--- Anni